import { StyleSheet } from 'react-native'


export default StyleSheet.create({
    suggestionItem: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: "100%",        
        backgroundColor: '#fff',
        color: 'rgba(0, 0, 0, 0.1)',
        height: 50,
        paddingHorizontal: 20,        
        borderBottomWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.05)',
    },
    text: {
        alignSelf: 'center',
        marginLeft: 12,
    },
    title: {
        fontSize: 16,
        color: 'rgba(0, 0, 0, 0.8)',
    },
    thumbnailWrapper: {
        width: 35,
        height: 35,
    },
    thumbnailChar: {
        fontSize: 16
    },  
    profileUserImage: {
        width: 40,
        minHeight: 40,
        borderRadius: 40 / 2,
        borderColor: "#ffffff",
        borderWidth: 3
    },
    noAvatarImage: {
        width: 40,
        minHeight: 40,
        borderRadius: 40 / 2,
        borderColor: "#ffffff",
        borderWidth: 3,
        backgroundColor: "#7E3532",
        alignItems: "center",
        justifyContent: "center",
        shadowColor: "#000000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.7
    },
    noAvatarText: {
        fontFamily: "FjallaOne-Regular",
        fontWeight: "bold",
        color: "white",
        fontSize: 20
    },
    userImageView: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        borderColor: "#222222",
        borderWidth: 1,
        overflow: "hidden"
    },
    userImage: {
        width: 30,
        height: 30
    },
    noAvatarImage: {
        width: 30,
        minHeight: 30,
        borderRadius: 30 / 2,
        borderColor: "#222222",
        borderWidth: 1,
        backgroundColor: "#7E3532",
        alignItems: "center",
        justifyContent: "center",
        overflow: "hidden"
    },
    noAvatarText: {
        fontFamily: "FjallaOne-Regular",
        fontWeight: "bold",
        color: "white",
        fontSize: 12
    },
    userName: {
        justifyContent: "center",
        marginLeft: 8,
        fontFamily: "FjallaOne-Regular"
    },
})
