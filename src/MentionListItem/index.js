import React from 'react'
import PropTypes from 'prop-types';
import {
    Text,
    View,
    Image,
    TouchableOpacity,
} from 'react-native'

// Styles
import styles from './MentionListItemStyles';

import Avatar from "../Avatar";

export class MentionListItem extends React.PureComponent {
    static propTypes = {
        item: PropTypes.object,
        onSuggestionTap: PropTypes.func,
        editorStyles: PropTypes.object,
    }
 
    onSuggestionTap = (user, hidePanel) => {
        this.props.onSuggestionTap(user);
    }

    render (){
        const { item: user, index, editorStyles } = this.props;
        return (
            <View >
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={[styles.suggestionItem, editorStyles.mentionListItemWrapper]}
                    onPress={() => this.onSuggestionTap(user)}>

                    <View style={{flexDirection:'row'}}>
                        {this.handleUserAvatar(user)}
                        <View style={[styles.text, editorStyles.mentionListItemTextWrapper]}>
                            <Text style={[styles.username, editorStyles.mentionListItemUsername]}>
                                @{user.username}
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }    

    handleUserAvatar(item) {
        if (item === null) {
          return null;
        }
    
        let avatar = item.avatar;
    
        if (avatar.trim() == "") {
          return (
            <View style={styles.noAvatarImage}>
              <Text style={styles.noAvatarText}>
                {item.username.slice(0, 1).toUpperCase()}
              </Text>
            </View>
          );
        } else {
          return (
            <View style={styles.userImageView}>
              <Image
                resizeMode="contain"
                style={styles.userImage}
                source={{ uri: avatar }}
              />
            </View>
          );
        }
      }
}

export default MentionListItem;